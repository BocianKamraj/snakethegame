package Main;

import java.awt.*;
import java.awt.geom.*;

import javax.swing.JPanel;

public class MyPanel extends JPanel {
    public int x_size = 10;
    public int y_size = 10;
    private int elem_size = 10;
    private Rectangle2D rectangle;

    public MyPanel() {
        setPreferredSize(new Dimension(200, 200));
        // prostokat
        rectangle = new Rectangle2D.Double(x_size, y_size,elem_size,elem_size);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.draw(rectangle);
    }

    public void setPosition(int X, int Y){
        x_size += X;
        y_size += Y;
        rectangle.setRect(x_size, y_size,elem_size,elem_size);
    }
}