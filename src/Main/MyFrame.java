package Main;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyListener;
import java.util.concurrent.TimeUnit;

import static java.lang.Math.abs;

public class MyFrame extends JFrame implements KeyListener{
    public int head_direction;
    public MyPanel panel;

    public MyFrame() {  //c-tor
        super("The Snake");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setSize(500,300);
        setLocation(50,50);
        head_direction = 39;

        panel = new MyPanel();
        add(panel);
        addKeyListener(this);
        setVisible(true);
    }

    public void moveSnakeConstantly() {
        while (true){
            moveSnake(head_direction);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent evt) {
        int key_pressed = evt.getKeyCode();
//        if (head_direction - key_pressed == 0) return; // pressed the same direction - > break
//        else if (abs(head_direction - key_pressed) == 2) return; // pressed opposite direction -> break
//        else  moveSnake(key_pressed); // everything is ok
        moveSnake(key_pressed);
    }

    public void moveSnake(int direction){
        switch(direction){
            case 40:
                head_direction = 40;
                System.out.println("down arrow");
                panel.setPosition(0,10);
                panel.repaint();
                break;
            case 39:
                head_direction = 39;
                panel.setPosition(10,0);
                System.out.println("right arrow");
                panel.repaint();
                break;
            case 38:
                head_direction = 38;
                panel.setPosition(0,-10);
                System.out.println("up arrow");
                panel.repaint();
                break;
            case 37:
                head_direction = 37;
                panel.setPosition(-10,0);
                System.out.println("left arrow");
                panel.repaint();
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
